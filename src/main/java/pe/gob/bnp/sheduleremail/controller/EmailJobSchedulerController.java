package pe.gob.bnp.sheduleremail.controller;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.UUID;

import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.gob.bnp.sheduleremail.dto.ScheduleEmailResponse;
import pe.gob.bnp.sheduleremail.job.EmailJob;

@RestController
public class EmailJobSchedulerController {

	private static final Logger logger = LoggerFactory.getLogger(EmailJobSchedulerController.class);

	@Autowired
	private Scheduler scheduler;

	@GetMapping("/scheduleEmail")
	public ResponseEntity<ScheduleEmailResponse> scheduleEmail() {
		try {
			ScheduleEmailResponse scheduleEmailResponse;
			ZonedDateTime dateTime = ZonedDateTime.of(LocalDateTime.now(), ZoneId.systemDefault());

			JobDetail jobDetail = buildJobDetail();

			Trigger trigger = buildJobTrigger(jobDetail, dateTime);

			scheduler.scheduleJob(jobDetail, trigger);

			scheduleEmailResponse = new ScheduleEmailResponse(true, jobDetail.getKey().getName(),
					jobDetail.getKey().getGroup(), "Email Scheduled Successfully!");

			return ResponseEntity.ok(scheduleEmailResponse);
			// } catch (SchedulerException ex) {
		} catch (Exception ex) {
			logger.error("Error scheduling email", ex);

			ScheduleEmailResponse scheduleEmailResponse = new ScheduleEmailResponse(false,
					"Error scheduling email. Please try later!");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(scheduleEmailResponse);
		}
	}

	private JobDetail buildJobDetail() {
		JobDataMap jobDataMap = new JobDataMap();

		jobDataMap.put("email", "desarrollo06.edsi@bnp.gob.pe");
		jobDataMap.put("subject", "facturador SUNAT");
		jobDataMap.put("body", "Error en servicios del facturador de la SUNAT...");

		return JobBuilder.newJob(EmailJob.class).withIdentity(UUID.randomUUID().toString(), "email-jobs")
				.withDescription("Send Email Job").usingJobData(jobDataMap).storeDurably().build();
	}

	private Trigger buildJobTrigger(JobDetail jobDetail, ZonedDateTime startAt) {
		return TriggerBuilder.newTrigger().forJob(jobDetail)
				.withIdentity(jobDetail.getKey().getName(), "email-triggers").withDescription("Send Email Trigger")
				.startAt(Date.from(startAt.toInstant()))
				// .withSchedule(SimpleScheduleBuilder.simpleSchedule().withMisfireHandlingInstructionFireNow())
				.withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInMinutes(2).repeatForever()).build();
	}

}
