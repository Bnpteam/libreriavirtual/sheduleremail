package pe.gob.bnp.sheduleremail.job;

import java.nio.charset.StandardCharsets;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import pe.gob.bnp.sheduleremail.dto.EmailData;
import pe.gob.bnp.sheduleremail.repository.FacturadorRepository;

@Component
public class EmailJob extends QuartzJobBean {
    private static final Logger logger = LoggerFactory.getLogger(EmailJob.class);

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private MailProperties mailProperties;    
	
	@Autowired
	FacturadorRepository facturadorRepository;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        logger.info("Executing Job with key {}", jobExecutionContext.getJobDetail().getKey());

        JobDataMap jobDataMap = jobExecutionContext.getMergedJobDataMap();
        String subject = jobDataMap.getString("subject");
        String body = jobDataMap.getString("body");
        String recipientEmail = jobDataMap.getString("email");
        
 	   
        EmailData resultFacturador=facturadorRepository.dataFacturador("dateSunat");
 		if(resultFacturador!=null ) {
 			if(resultFacturador.getTo()!=null && resultFacturador.getTo().length()>0) {
 				 recipientEmail = resultFacturador.getTo().toString();
 	 			 body = resultFacturador.getContent().toString();
 	 			 subject = resultFacturador.getSubject().toString();
 	 			 sendMail(mailProperties.getUsername(), recipientEmail, subject, body,resultFacturador.getId());
 			} 			
 		}
       
    }

    private void sendMail(String fromEmail, String toEmail, String subject, String body,String id) {
        try {
            logger.info("Sending Email to {}", toEmail);
            MimeMessage message = mailSender.createMimeMessage();

            MimeMessageHelper messageHelper = new MimeMessageHelper(message, StandardCharsets.UTF_8.toString());
            messageHelper.setSubject(subject);
            messageHelper.setText(body, true);
            messageHelper.setFrom(fromEmail);
            messageHelper.setTo(InternetAddress.parse(toEmail));            
            messageHelper.setBcc("desarrollo06.edsi@bnp.gob.pe");

            mailSender.send(message);
            
            facturadorRepository.updateState(id);
        } catch (MessagingException ex) {
            logger.error("Failed to send email to {}", toEmail);
        }
    }
}
