package pe.gob.bnp.sheduleremail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SheduleremailApplication {

	public static void main(String[] args) {
		SpringApplication.run(SheduleremailApplication.class, args);
	}

}
