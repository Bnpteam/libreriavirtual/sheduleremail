package pe.gob.bnp.sheduleremail.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.sheduleremail.dto.EmailData;
import pe.gob.bnp.sheduleremail.repository.BaseJDBCOperation;
import pe.gob.bnp.sheduleremail.repository.FacturadorRepository;

@Repository
public class FacturadorJDBC extends BaseJDBCOperation implements FacturadorRepository {

	private static final Logger logger = LoggerFactory.getLogger(FacturadorRepository.class);


	@Override
	public EmailData dataFacturador(String type) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_UTILITY.SP_GET_EMAIL_ERROR(?,?)}";
        
        EmailData ubigeoResponse = new EmailData();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getSqliteConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
		
			if (codigoResultado.equals("0000")) {
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				if (rs.next()) {	
					ubigeoResponse = new EmailData();
					ubigeoResponse.setId(rs.getString("EMAIL_ID"));
					ubigeoResponse.setSubject(rs.getString("EMAIL_SUBJECT"));
					ubigeoResponse.setContent(rs.getString("EMAIL_CONTENT"));
					ubigeoResponse.setTo(rs.getString("EMAIL_TO"));
					
				}else {
					logger.info("No hay datos..");
				}
			}else {
				logger.error("PKG_API_UTILITY.SP_GET_EMAIL_ERROR2:");
			}
		}catch(SQLException e) {
			logger.error("PKG_API_UTILITY.SP_GET_EMAIL_ERROR: "+e.getMessage());			
			
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return ubigeoResponse;
	}


	@Override
	public void updateState(String id) {
		// TODO Auto-generated method stub
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_UTILITY.SP_SEND_EMAIL_REEV(?,?)}";/*8*/
             
      //  List<Object> usersResponse = new ArrayList<>();
      //  ResponseTransaction response = new ResponseTransaction();
      
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getSqliteConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("P_ORDER_EMAIL",id);			
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);	
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			//response.setCodeResponse(codigoResultado);
			if (codigoResultado.equals("0000")) {
				cn.commit();
				//response.setResponse(messageSuccess);
				//rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				//((if (rs.next()) {
				//	CalculateResponse userResponse = new CalculateResponse(); 
				//	userResponse.setSubtotalCost(Utility.getString(rs.getString("AMOUNT")));
				//	userResponse.setShippingCost(Utility.getString(rs.getString("SHIPPING_COST")));
				//	userResponse.setIgvCost(Utility.getString(rs.getString("IGV_COST")));
				//	userResponse.setTotalCost(Utility.getString(rs.getString("TOTAL_COST")));
				
					
				//	usersResponse.add(userResponse);
				//}	
				//response.setList(usersResponse);			
			}else {
				cn.rollback();
			}
		}catch(SQLException e) {
			logger.error("PKG_API_UTILITY.SP_SEND_EMAIL: "+e.getMessage());
		
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		
	}

}

