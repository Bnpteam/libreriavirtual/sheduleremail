package pe.gob.bnp.sheduleremail.repository;

import java.sql.Connection;
import java.sql.DriverManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class SqliteConnection {
	
	private static final Logger logger = LoggerFactory.getLogger(SqliteConnection.class);	
	
	private static String catchConecctionJDBC="getConnectionJDBC ";	
	
	private static String userDB="TIENDA_VIRTUAL";
	
	private static String passwordDB="tiendvirut43dewx1";

	public static Connection getConnectionJDBC() throws Exception {
		String connectionURL = "jdbc:oracle:thin:@172.16.89.202:1521:dbprod";	//prod
		//String connectionURL =   "jdbc:oracle:thin:@172.16.8.87:1521:dbqa";	//qa
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(connectionURL, userDB,passwordDB);
			connection.setAutoCommit(false);
		} catch (Exception exception) {
			logger.error(catchConecctionJDBC + exception.getMessage(), exception);
			
		}
		return connection;
	}

}

