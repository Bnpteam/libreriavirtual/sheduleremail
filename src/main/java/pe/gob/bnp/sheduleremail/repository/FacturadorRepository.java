package pe.gob.bnp.sheduleremail.repository;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.sheduleremail.dto.EmailData;

@Repository
public interface FacturadorRepository {
	public EmailData dataFacturador(String dateSunat);
	public void updateState(String id);
}
