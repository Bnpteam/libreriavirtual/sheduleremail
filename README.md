## SERVICIO NOTIFICACIONES LIBRERIA VIRTUAL
### Descripción
- Backend de servicios Notificaciones Libreria Virtual.

### Configuración

- Para configurar en el ambientes de TEST:

 * Dentro del Proyecto sheduleremail cambiar SqliteConnection.class a la conexion de TEST.
 * IDE - STS 4.0.
 * TOMCAT 9.0 EMBEBIDO.
 * JDK 1.8.0.261
 * RUTA:http://172.16.89.199:9881/scheduleEmail (ejecución interna)
   
- Para configurar en el ambiente de PROD:

 * Dentro del Proyecto sheduleremail cambiar SqliteConnection.class a la conexion de PROD.
 * IDE - STS 4.0.
 * TOMCAT 9.0 EMBEBIDO.
 * JDK 1.8.0.261
 * RUTA:http://172.16.89.199:9881/scheduleEmail (ejecución interna)
 

### Despliegue Microservicio

- Utilizar el archivo sheduleremail-be.service como base para desplegar el microservicio
- Pasos de despliegue:
    1. Generar jar del aplicativo sheduleremail (mave clean build install).
    2. Copiar el jar dentro del servidor test o prod que desee en la ruta indicada por el archivo sheduleremail-be.service. (dar permisos escritura y lectura al jar)
    3. Generar en caso no exista un servicio en linux con el nombre del archivo sheduleremail-be.service.
        * Para crear servicio en linux, acceder a la ruta /etc/systemd validar los servicios existentes y copiar con el comando "cp" servicio existente actualizar       las rutas para que apunten al jar creado. 
    4. Una vez creado o actualizado el archivo .service reiniciarlo de preferencia utilizar los comando:
        systemctl status sheduleremail-be.service (ver el estado actual)
        systemctl stop sheduleremail-be.service (detener servicio)
        systemctl start sheduleremail-be.service (inciar servicio)
    
    5. Finalmente validar los servicios desde la ruta generado por el swagger.

 
#### version: v1.0.2